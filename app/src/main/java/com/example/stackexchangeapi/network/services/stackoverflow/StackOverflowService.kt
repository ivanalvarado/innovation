package com.example.stackexchangeapi.network.services.stackoverflow

import retrofit2.Call
import retrofit2.http.GET

interface StackOverflowService {
    @GET("/2.2/users?order=desc&sort=reputation&site=stackoverflow")
    fun getStackOverflowUsers(): Call<StackOverflowUsersResponse>
}