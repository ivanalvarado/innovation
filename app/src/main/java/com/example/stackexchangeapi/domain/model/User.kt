package com.example.stackexchangeapi.domain.model

data class User(
    val id: String
)