package com.example.stackexchangeapi.util

import android.util.Log

object Logger {

    @JvmOverloads fun error(tag: String?, message: String?, error: Throwable? = null) {
        Log.e(tag, message ?: "No Message", error)
    }

    fun warning(tag: String?, message: String?) {
        Log.w(tag, message ?: "No Message")
    }

    fun debug(tag: String?, message: String?) {
        Log.d(tag, message ?: "No Message")
    }

    fun info(tag: String?, message: String?) {
        Log.i(tag, message ?: "No Message")
    }

    fun verbose(tag: String?, message: String?) {
        Log.v(tag, message ?: "No Message")
    }
}