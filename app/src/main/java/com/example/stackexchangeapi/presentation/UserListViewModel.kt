package com.example.stackexchangeapi.presentation

import android.arch.lifecycle.ViewModel
import com.example.stackexchangeapi.domain.usecase.IUserPostUseCase
import com.example.stackexchangeapi.domain.usecase.UserPostUseCase
import javax.inject.Inject

class UserListViewModel @Inject constructor(
    private val userPostUseCase: IUserPostUseCase
) : ViewModel() {

    private lateinit var userId: String

    fun init(userId: String) {
        this.userId = userId
    }

    fun getUsers() = userPostUseCase.getUsers()

    fun getUserPosts() = userPostUseCase.getPostsWith(userId)
}