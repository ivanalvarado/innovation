package com.example.stackexchangeapi.domain.repository

import com.example.stackexchangeapi.domain.model.Post

interface PostRepository {

    fun getPostsWith(userId: String): List<Post>
}