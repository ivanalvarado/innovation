package com.example.stackexchangeapi

import android.app.Application
import com.example.stackexchangeapi.di.components.AppComponent
import com.example.stackexchangeapi.di.components.DaggerAppComponent
import com.example.stackexchangeapi.di.modules.AppModule

class MainApplication : Application() {

    val component: AppComponent by lazy {
        DaggerAppComponent.builder().appModule(AppModule(this)).build()
    }

    override fun onCreate() {
        super.onCreate()
        component.inject(this)
    }
}