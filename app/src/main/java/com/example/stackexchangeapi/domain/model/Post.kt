package com.example.stackexchangeapi.domain.model

data class Post(
    val id: String
)