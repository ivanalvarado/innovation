package com.example.stackexchangeapi.di.modules

import com.example.stackexchangeapi.domain.repository.PostRepository
import com.example.stackexchangeapi.domain.repository.UserRepository
import com.example.stackexchangeapi.repositories.PostRepositoryImpl
import com.example.stackexchangeapi.repositories.UserRepositoryImpl
import dagger.Binds
import dagger.Module

@Module
abstract class RepositoryModule {

//    @Binds
//    abstract fun bindsPostRepository(repository: PostRepositoryImpl): PostRepository{
//        PostRepositoryImpl()
//    }

    @Binds
    abstract fun bindsUserRepository(repository: UserRepositoryImpl): UserRepository
}