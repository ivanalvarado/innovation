package com.example.stackexchangeapi.di.modules

import android.arch.lifecycle.ViewModel
import com.example.stackexchangeapi.di.keys.ViewModelKey
import com.example.stackexchangeapi.presentation.UserListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(UserListViewModel::class)
    abstract fun bindUserListViewModel(userListViewModel: UserListViewModel): ViewModel
}