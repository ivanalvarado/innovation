package com.example.stackexchangeapi.di.components

import android.app.Application
import com.example.stackexchangeapi.di.modules.AppModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun inject(app: Application)
}