package com.example.stackexchangeapi.domain.usecase

import com.example.stackexchangeapi.domain.model.Post
import com.example.stackexchangeapi.domain.model.User
import com.example.stackexchangeapi.domain.repository.PostRepository
import com.example.stackexchangeapi.domain.repository.UserRepository
import javax.inject.Inject

interface IUserPostUseCase {
    fun getUserPosts()
    fun getUsers(): List<User>
    fun getPostsWith(userId: String): List<Post>
}

class UserPostUseCase @Inject constructor(
    private val userRepository: UserRepository,
    private val postRepository: PostRepository
) : IUserPostUseCase {

    override fun getUserPosts() {

    }

    override fun getUsers(): List<User> {
        return userRepository.getUsers()
    }

    override fun getPostsWith(userId: String): List<Post> {
        return postRepository.getPostsWith(userId)
    }
}