package com.example.stackexchangeapi.domain.repository

import com.example.stackexchangeapi.domain.model.User

interface UserRepository {

    fun getUsers(): List<User>

    fun getUserWith(userId: String): User
}